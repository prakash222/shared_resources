package com.example.testing_applyjob;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.chip.adapter.ChipBuilder;
import com.chip.view.ChipView;

import me.gujun.android.taggroup.TagGroup;

public class MainActivity extends AppCompatActivity {

    TagGroup mTagGroup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ChipView chipView = (ChipView) findViewById(R.id.chipView);



    }
    public class Chip extends ChipBuilder {

        @Override
        public View getChip(LayoutInflater inflater, String data) {
            View view = inflater.inflate(R.layout.activity_main, null);
            TextView txtChip = (TextView) view.findViewById(R.id.chipView);
            txtChip.setText(data);
            return view;
        }
    }



}
